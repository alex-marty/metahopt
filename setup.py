#!/usr/bin/env python

"""setup.py shim, only here to allow for editable installs"""

import setuptools

if __name__ == "__main__":
    setuptools.setup()
